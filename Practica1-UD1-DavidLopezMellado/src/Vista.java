/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
import base.Habitacion;
import org.w3c.dom.*;
import org.xml.sax.HandlerBase;
import org.xml.sax.SAXException;

import javax.management.JMException;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {
    private JFrame frame;
    private JPanel panel1;
    private JTextField nombreTxt;
    private JTextField direccionTxt;
    private JComboBox comboBox;
    private JButton altaHabitacionButton;
    private JButton mostrarHabitacionButton;
    private JLabel lblNombreCliente;
    private JLabel lblDireccionCliente;
    private JLabel lblHabitacion;
    private JTextField nacionalidadTxt;
    private JTextField numeroTelefonoTxt;
    private JButton borrarCamposBtn;


    //Elementos añadidos por mi
    private LinkedList<Habitacion> lista;
    private DefaultComboBoxModel<Habitacion> dcbm;


    public Vista() {
        frame = new JFrame("Vista");
        //frame.setContentPane(new Vista().panel1);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        crearMenu();
        frame.setVisible(true);

        frame.setBounds(300,300,500,400);
        frame.setLocationRelativeTo(null);

        lista= new LinkedList<>();
        dcbm= new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);



        altaHabitacionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                altaHabitacion(nombreTxt.getText(),direccionTxt.getText(),nacionalidadTxt.getText(),numeroTelefonoTxt.getText());
                refrescarComboBox();
            }
        });

        mostrarHabitacionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Habitacion seleccionado = (Habitacion) dcbm.getSelectedItem();
                lblHabitacion.setText(seleccionado.toString());
            }
        });



        borrarCamposBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nombreTxt.setText("");
                direccionTxt.setText("");
                nacionalidadTxt.setText("");
                numeroTelefonoTxt.setText("");
            }
        });
    }

    /**
     * Método que nos permite borrar todo lo que tenga almacenado el defaulComboboxModel y
     * rellenarlo con el contenido del LinkedList lista
     */

    private void refrescarComboBox(){
        dcbm.removeAllElements();
        for(Habitacion habitacion: lista){
            dcbm.addElement(habitacion);
        }
    }


    /**
     * Método que nos permite agregar una habitacion
     * @param nombreCliente nombre del cliente
     * @param direccionCliente direccion del cliente
     * @param nacionalidadCliente nacionalidad del cliente
     * @param numeroTelefonoCliente numero telefono del cliente
     */
    private void altaHabitacion(String nombreCliente, String direccionCliente,String nacionalidadCliente,
                                String numeroTelefonoCliente){
        lista.add(new Habitacion(nombreCliente,direccionCliente,nacionalidadCliente,numeroTelefonoCliente));
    }

    /**
     * Método que nos permite crear una barra de menu y un menu desplegable con las opciones
     * "Exportar XML" e "Importar XML"
     */
    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if(opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });



        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if(opcion == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);
        barra.add(menu);

        frame.setJMenuBar(barra);
    }


    /**
     * Método que nos permitirá importar un archivo XML con los datos que haya en
     * en un archivo de tipo XML
     *
     * @param fichero variable fichero que recibe el método
     */

    private void importarXML(File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);


            NodeList habitaciones = documento.getElementsByTagName("habitacion");
            for(int i=0; i<habitaciones.getLength();i++){
                Node habitacion = habitaciones.item(i);
                Element elemento = (Element) habitacion;

                String nombreCliente = elemento.getElementsByTagName("nombreCliente").item(0).getChildNodes().item(0).getNodeValue();
                String direccionCliente = elemento.getElementsByTagName("direccionCliente").item(0).getChildNodes().item(0).getNodeValue();

                String nacionalidadCliente = elemento.getElementsByTagName("nacionalidadCliente").item(0).getChildNodes().item(0).getNodeValue();
                String numeroTelefonoCliente = elemento.getElementsByTagName("numeroTelefonoCliente").item(0).getChildNodes().item(0).getNodeValue();

                altaHabitacion(nombreCliente,direccionCliente,nacionalidadCliente,numeroTelefonoCliente);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    /**
     * Método que nos permitirá exportar los archivos en un archivo XML que hayamos creado en el programa.
     * @param fichero variable fichero que recibe el método
     */
    private void exportarXML(File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try{

            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();


            Document documento = dom.createDocument(null, "xml", null);
            Element raiz = documento.createElement("habitaciones");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoHabitacion;
            Element nodoDatos;
            Text dato;


            for(Habitacion habitacion : lista){


                nodoHabitacion = documento.createElement("habitacion");
                raiz.appendChild(nodoHabitacion);


                nodoDatos = documento.createElement("nombreCliente");
                nodoHabitacion.appendChild(nodoDatos);

                dato = documento.createTextNode(habitacion.getNombreCliente());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("direccionCliente");
                nodoHabitacion.appendChild(nodoDatos);

                dato=documento.createTextNode(habitacion.getDireccionCliente());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("nacionalidadCliente");
                nodoHabitacion.appendChild(nodoDatos);

                dato=documento.createTextNode(habitacion.getNacionalidadCliente());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("numeroTelefonoCliente");
                nodoHabitacion.appendChild(nodoDatos);

                dato=documento.createTextNode(habitacion.getNumeroTelefonoCliente());
                nodoDatos.appendChild(dato);

            }

            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);


            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src,result);




        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }


    }

    /**
     * Método que nos recoge el objeto vista
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
    }
}
