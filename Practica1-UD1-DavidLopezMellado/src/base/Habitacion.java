/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package base;

public class Habitacion {

    String nombreCliente;
    String direccionCliente;
    String nacionalidadCliente;
    String numeroTelefonoCliente;


    public Habitacion(){

    }

    public Habitacion(String nombreCliente, String direccionCliente, String nacionalidadCliente, String numeroTelefonoCliente){
        this.nombreCliente=nombreCliente;
        this.direccionCliente=direccionCliente;
        this.nacionalidadCliente=nacionalidadCliente;
        this.numeroTelefonoCliente=numeroTelefonoCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDireccionCliente() {
        return direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    public String getNacionalidadCliente() {
        return nacionalidadCliente;
    }

    public void setNacionalidadCliente(String nacionalidadCliente) {
        this.nacionalidadCliente = nacionalidadCliente;
    }

    public String getNumeroTelefonoCliente() {
        return numeroTelefonoCliente;
    }

    public void setNumeroTelefonoCliente(String numeroTelefonoCliente) {
        this.numeroTelefonoCliente = numeroTelefonoCliente;
    }

    @Override
    public String toString() {
        return "Habitacion{" +
                "nombreCliente='" + nombreCliente + '\'' +
                ", direccionCliente='" + direccionCliente + '\'' +
                ", nacionalidadCliente='" + nacionalidadCliente + '\'' +
                ", numeroTelefonoCliente='" + numeroTelefonoCliente + '\'' +
                '}';
    }
}
